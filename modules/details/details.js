'use strict';

angular.module('myApp.modules.details', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/details', {
    templateUrl: 'modules/details/details.html',
    controller: 'DetailsCtrl'
  });
}])
.factory('members', function($http) {

	return {
		get:function() {
			return $http.get('jsons/members.json');
		}
	};
})
.controller('PopoverTaskCtrl', function ($scope) {
  $scope.dynamicPopover = {
    templateUrl: 'modules/details/add_new_task.html',
    title: 'Create Task'
  };

})
.controller('DetailsCtrl', function($scope,$rootScope,$routeParams,members) {
	$rootScope.title=$routeParams.project_name;
	$scope.name = $routeParams.project_name;

    members.get().then(function(member){
		$scope.models=member.data[$scope.name];
	});
});


function changeTaskStatus(statusSelect)
{
	var parentLi = $(statusSelect).closest("li").attr("id");
	var parentUl=$(statusSelect).closest("ul").attr("id");
	
	var status=statusSelect.value;
	var color="#20B796";
	if(status=='Done')
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width greenColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #20B796");
		color="#20B796";
	}
	else if(status=='Sent')
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width blueColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #337ab7")
		color="#337ab7";
	}
	else if(status=='In Process')
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width skyColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #f0ad4e")
		color="#f0ad4e";
	}
	else if(status=='On Hold')
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width orangeColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #5bc0de")
		color="#5bc0de";
	}
	else if(status=='Schedule')
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width redColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #d9534f")
		color="#d9534f";
	}
	else
	{
		statusSelect.setAttribute("class", "btn-rounded btn-xsm m-t-10 btn-width greenColor");
		//li_element.setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px #20B796")
		color="#20B796";
	}
	changeColor(parentLi,parentUl,color);

}

function changeColor(parentLi,parentUl,color)
{
	var ul_element = document.getElementById(parentUl);
	var items = ul_element.getElementsByTagName("li");
	for (var i = 0; i < items.length; ++i) {
  		if(items[i].id==parentLi)
  		{
  			items[i].setAttribute("style","background-color:#F7F7F9;border-radius:2px;border: 1px;margin-top: 15px;border-left: solid 3px "+color);
  	}

	}
}