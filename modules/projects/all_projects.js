'use strict';

angular.module('myApp.modules.projects', ['ngRoute','ngAnimate', 'ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/all_projects', {
    templateUrl: 'modules/projects/all_projects.html',
    controller: 'AllProjectsCtrl'
  });
}])
.factory('myFactory', function($http) {

	return {
		get:function() {
			return $http.get('jsons/projects.json');
		}
	};
})
.controller('AllProjectsCtrl', function($scope,myFactory) {
	myFactory.get().then(function(msg){
		$scope.projects_data=msg.data.projects;
	});
})
.controller('PopoverDemoCtrl', function ($scope) {
  $scope.dynamicPopover = {
    templateUrl: 'modules/projects/add_new_project.html',
    title: 'Create Project'
  };

});