'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'dndLists',
  'myApp.modules.projects',
  'myApp.modules.details'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/all_projects'});
}])
.controller('TitleCtrl', function($rootScope) {
	$rootScope.title = "Task Management";
});
