## Sprinklr UI Assignment

#### Running the Project:

git clone repo-url https://pushkard02@bitbucket.org/pushkard02/assignment-ui.git
npm install -g http-server
http-server &

`http://localhost:{port}/Assignment-UI/index.html`

#### Dependecies:
###### Technologies Used:
- AngularJs v1.5.8
- Bootstrap v3.3.7
###### Libraries Used:
- [angular-ui-bootstrap](https://angular-ui.github.io/bootstrap/)
- [angular-drag-and-drop-lists](http://marceljuenemann.github.io/angular-drag-and-drop-lists/demo/#/nested)

#### Structure:

├── app.css  base css classes that is used commonly in app
├── app.js  base for project 
├── index.html landing page for web 
├── jsons
│   ├── members.json   modal of members in each project and other related information.
│   └── projects.json  modal of all projects and it's description
├── libs
│   ├── angular  
│   ├── bootstrap
│   ├── common  contains CSS and Js which are commonly used like Jquery.js
│   └── font-awesome for icons 
│       ├── css
│       └── fonts
└── modules
    ├── details
    │   ├── add_new_task.html   view for  create new task popover
    │   ├── details.css  project details views  css 
    │   ├── details.html     views that shows the details of each project like task assigned to each members and tasks are draggable etc.
    │   └── details.js  contains services and controllers for detalils page.
    └── projects
        ├── add_new_project.html add new project popover view 
        ├── all_projects.css 
        ├── all_projects.html views for all the projects and its description
        └── all_projects.js ui side services and controllers for all project